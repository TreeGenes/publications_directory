Installation
============

Installation of this module is fairly straight forward. Clone this current repository into your Tripal's 
module directory such as sites/all/modules/publications_directory.

* Make sure you have publications already published within your chado database.

* Go to admin/modules and enable Publications Directory

* Then visit /publications_directory/search 

 