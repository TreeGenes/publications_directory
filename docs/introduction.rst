Introduction
============

The Publications Directory is a customized search page intended to display literature / publications in specific 
way. It was initially designed for TreeGenes as a replacement for a 'view' since we required a more complex search 
as well as a more complex display. We hope that this module will be useful. Please keep in mind that display output 
requires manual modification thus further updates can overwrite your current modifications. Please keep a backup 
before updating.
