Requirements
============

The following are the minimum basic requirements to get Publications Directory up and running:

* You need a Drupal website

* You need Tripal v3 installed on your Drupal website

* You need to install Chado v1.3

* You must have publications installed in CHADO

* You need the Publications Directory files by cloning the repo.