Modifications
=============

The Publications Directory search page is generated entirely by code instead of using the Views API. Thus in order 
to currently modify the display of Publications Directory, you will have to modify the code produced by the functions 
that are used to create the HTML. Please remember to backup your files after modification since updating from git 
could overwrite the changes you make (this will only happen if you decide to update to a newer version if one exists).

* function publications_directory_menu()
You can modify this function to change the url but you can also do this within the Admin UI.

* function publications_directory_generate_search_page
This function is the main function that generates the publications search page at /publications_directory/search.
Sub functions within this function call different sections of the page to produce the final page. Important sub 
functions will be described below.

* function publications_directory_generate_sortby_form()
This function produces the sort by fields which can be found under the Year from and to fields.

* function publications_directory_generate_search_form()
This function is used within publications_directory_generate_search_page function. It generates the search form 
at the top of the page. This function has sub functions which help to produce the search section of the Publications 
Directory page.

* function pubsearch_custom_addjs_searchitem_functions()
This function does the dynamic HTML / JS part of the page. It is used to add items dynamically to the search form.

* function publications_directory_generate_pager(...)
This function will add the pagination section of the page.

* function publications_directory_generate_fieldsearch_by_name()
This function will add a section to search by alphabetic name. We don't use it in the latest update but you can 
use this to search by first letter (A B C D etc).

* function publications_directory_generate_fieldsearch_by_yearbetween()
This function will generate the display for search between years in the search form.

* function publications_directory_add_searchitem_to_get_variable($item)
This function translates the items to search form and adds them to the url as GET variables. This function does 
this part of the magic.


