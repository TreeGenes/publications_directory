.. Publications Directory documentation master file, created by
   sphinx-quickstart on Fri Aug 30 22:20:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Publications Directory's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   requirements
   installation
   modifications
   tables

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
