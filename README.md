# Publications Directory

This module creates the Literature section used on TreeGenes. It can be used on any Tripal V3 website that contains the Publication Tripal Content Type. 

Table of contents
-----------------

* [Introduction](#introduction)
* [Installation](#installation)
* [Usage](#usage)
* [Known issues and limitations](#known-issues-and-limitations)
* [Getting help](#getting-help)
* [Contributing](#contributing)
* [License](#license)
* [Authors and history](#authors-and-history)
* [Acknowledgments](#authors-and-acknowledgments)

Introduction
------------

The Publications Directory was built to make a single page that is capable of doing complex multi-item searches on the data saved within the CHADO pub tables.
It can thus be used on any Tripal V3 website in which the Publication Tripal Content Type already exists. It was created due to the limitations of form api 
and also limitations of views. It was never meant to replace views but to provide a custom way of searching and presenting the data.

Installation
------------

* Please ensure you are already running a Tripal V3 website with CHADO V1.3 installed. This is usually the norm but may change in the future.
* Clone this GIT repo into a directory within your Tripal website such as /sites/all/modules/publications_directory
* Enable the module 'Publications Directory' in the Administration Module interface /admin/modules
* You may need to clear menu cache or clear the full cache which is probably easier
* Visit publications_directory/search on your website

Usage
-----

Usage is generally straight forward as long as your installation was successful. Visit publications_directory/search and the Publications Directory 
page should load. It will paginate all publication records automatically and generate the page. To the top of the page, you may use the search to filter 
the publications you are looking for.

Known Issues and Limitations
----------------------------

* Updating the display of the records within the directory requires you to know PHP and HTML. Since it does not use the Views UI, you have to manually 
change the code if you need to change the default display. Thus if there are other updates we push out via GIT, this could overwrite your manual changes.

Getting help
------------

An easy way of getting help is to open an issue right here within Gitlab under this project and we'll look into the issue(s).

Contributing
------------

Contributing is always welcomed, just edit and push and we'll review and apply accordingly. Please remember that if you would like a design change 
this will take a bit longer since we will have to adjust code so that our site (TreeGenes) keeps our look in the process as well.

License
-------

This project is licensed under the GPL license. A license file has been attached.


Authors and history
-------------------

Risharde Ramnath (risharde@gmail.com)

Acknowledgements
----------------

Special thanks to the University of Connecticut
TreeGenes and all funding sources

 

